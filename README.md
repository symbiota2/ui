# Symbiota2 Frontend

### Development
1. Run the [Symbiota2 API](https://gitlab.com/symbiota2/api)
2. Configure the API url in [environments](src/environments)
3. Install npm dependencies: `npm i`
4. Build the core and plugins: `npm run build`
5. Merge translations: `npm run i18n`
6. Start the dev server: `npm run start:dev`

To create a new plugin
```console
$ ng g library @myorg/ui-plugin-myplugin
```

To inject UI components from the plugin into Symbiota2 core, have your module extend the
abstract [SymbiotaPlugin](./plugins/symbiota2/ui-common/src/lib/plugin/symbiota-plugin.class.ts)
class. You can then inject components into the site's routes, navbar and user profile tabs by
overloading SymbiotaPlugin's static methods:

```typescript
    static getRoutes(): Route[] {
        return [{
            path: "collections",
            component: CollectionCheckboxSelectorComponent
        }];
    }
```

```typescript
    static getNavBarLinks(): NavBarLink[] {
        return [{
            name: "core.layout.header.topnav.search_collections_link",
            url: "/collections"
        }];
    }
```
```typescript
    static getUserProfileTabs(): UserProfileTab[] {
        return [
            {
                name: 'My Institutions',
                component: UserProfileInstitutionTab
            },
            {
                name: 'My collections',
                component: UserProfileCollectionTab
            },
        ];
    }
```

When the plugin is added to the core's [PLUGINS array](./src/app/app.module.ts#L28), 
these components are automatically loaded by the 
[PluginService](./plugins/symbiota2/ui-common/src/lib/plugin/plugin.service.ts)

To use other plugins within your own, add the plugin to your plugins's
[peer dependencies](https://nodejs.org/en/blog/npm/peer-dependencies/) and import any
needed resources:
```typescript
import { ApiClientService } from "@symbiota2/ui-common";
```
