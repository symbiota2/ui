export class LoginPostData {
    username: string;
    password: string;
}

export class LoginResponseData {
    accessToken: string;
}
